<?php
//use Inc\Header\Header;


class Helper
{
    public function session()
    {
        if(!$_SESSION['admin']["is_admin"])
        {
            header('Location: '.BASE_URL.'admin?m=login');
        }
    }

    public function loginProject($post='')
    {
        ini_set("display_errors", "1");
        error_reporting(E_ALL);
        if(isset($_POST['submit']))
        {
            $obj=new AuthController();
            $login_return_data=$obj->validateUserDetails($_POST['user_name'],$_POST['password']);
            if($login_return_data)
            {
                $_SESSION['admin']['is_admin']=1;
                header('Location: '.BASE_URL.'admin/?a=productGrid');

            }
            else
            {
                return "Invalid credentials";
            }
        }
    }

    public function productGrid()
    {
        $this->session();
        if(isset($_POST['createProduct']))
        {
            header('Location: '.BASE_URL.'admin/?a=createProduct');
        }
        if(isset($_POST['logout']))
        {   session_destroy();
            header('Location: '.BASE_URL.'admin/login.php?a=login');
        }
        return true;
    }



    /**
     *
     */
    public function createProduct()
    {
        $this->session();
        if(isset($_POST['logout']))
        {   session_destroy();
            header('Location: '.BASE_URL.'admin/login.php');
        }

        if(isset($_POST['product']))
        {
            header('Location: '.BASE_URL.'admin/?a=productGrid');
        }
        $obj=new ProductController();
        if(isset($_POST['submit']))
        {
            return $obj->validateProduct($_POST);

        }

    }

    public function editProduct()
    {
        if(!$_SESSION["is_admin"])
        {
            header('Location: /shopping/admin/login.php');
        }
        $obj=new ProductController();
        if(isset($_POST['logout']))
        {   session_destroy();
            header('Location: '.BASE_URL.'admin/login.php');
        }

        if(isset($_POST['product']))
        {
            header('Location: '.BASE_URL.'admin/productGrid.php');
        }
        if(isset($_GET['id'])) {
            $obj = new ProductController();
            $product_single_data = $obj->getSingleProductByID($_GET['id']);
            if (isset($_POST['submit'])) {
                $response = $obj->validateProduct($_POST,1,$_GET['id']);
                $product_single_data = $obj->getSingleProductByID($_GET['id']);
            }
            return $product_single_data;
        }
    }


}
?>