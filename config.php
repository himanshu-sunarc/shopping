<?php
ini_set("display_errors", "0");
error_reporting(E_ALL);
define('BASE_URL','http://'.$_SERVER['SERVER_NAME'].'/shopping/');
define('UPLOAD_PATH',__DIR__.'/public/images/');
define('CSS_PATH',BASE_URL.'public/css/');
define('JS_PATH',BASE_URL.'/public/js/');
define('MAIL',__DIR__.'/vendor/phpmailer/phpmailer/src/');
define('IMAGE_SHOW',BASE_URL.'/public/images/');
define('HOST','localhost');
define('USER','root');
define('PASSWORD','');
define('DATABASE','shopping');
