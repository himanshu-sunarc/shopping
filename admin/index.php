<?php
/*include("../include.php");
if($_SESSION["is_admin"])
{
    header('Location: '.BASE_URL.'view/admin/productGrid.php?adminlogin=1');
}
else{
    header('Location: '.BASE_URL.'view/admin/login.php?adminlogin=0');
}*/
$model = '';
if(isset($_GET['a'])){
    $model = $_GET['a'];
}
switch ($model)
{
    case "login":
        require_once (__DIR__."/../model/admin/login.php");

        break;

    case "logout":
        require_once (__DIR__."/../model/admin/logout.php");

        break;

    case "productGrid":
        require_once (__DIR__."/../model/admin/productGrid.php");

        break;

    case "createProduct":
        require_once (__DIR__."/../model/admin/createProduct.php");

        break;

    case "editProduct":
        require_once (__DIR__."/../model/admin/editProduct.php");

        break;

    case "deleteProduct":
        require_once (__DIR__."/../model/admin/deleteProduct.php");

        break;
    default:
        require_once (__DIR__."/../model/admin/login.php");

        break;
}
?>