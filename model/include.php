<?php
require_once(__DIR__."/../config.php");
require_once(__DIR__."/../controllers/AuthController.php");
require_once(__DIR__."/../controllers/dbcontroller.php");
require_once(__DIR__."/../controllers/ProductController.php");
require_once(__DIR__."/../controllers/SessionController.php");
require_once(__DIR__."/../controllers/CartController.php");
require_once(__DIR__."/../controllers/RegistrationController.php");
require_once(__DIR__."/../controllers/LoginController.php");
require_once(__DIR__."/../controllers/CheckUserController.php");
require_once(__DIR__."/../library/gump.class.php");
require_once(__DIR__."/../library/class.Cart.php");
require_once(__DIR__."/../helper/Helper.php");
require_once(__DIR__."/../view/admin/header.php");
require_once(__DIR__."/../view/admin/footer.php");

require __DIR__.'/../vendor/autoload.php';

?>