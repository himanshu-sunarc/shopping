<?php

include_once(__DIR__ . "/include.php");

if (isset($_POST['user_availibility_before_checkout_btn'])) {

    if (isset($_POST['user_already_available'])) {
        $checkUserObj = new CheckUserController();
        $checkUserObj->checkIfUserAvailable();
    }

    if (isset($_POST['user_not_available'])) {
        $registerControllerObj = new RegistrationController($_POST);

        $validated = $registerControllerObj->validateUser();

        $isEmailAvailableOrNot = $registerControllerObj->getEmail();
        $isEmailAvailableOrNot = $registerControllerObj->checkErrorNotAvailable();

    }
}
include_once(__DIR__ . "/../view/auth/checkuserview.php");