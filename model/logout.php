<?php

include_once (__DIR__."/include.php");


if (isset($_GET['m']) == 'logout')
{
    $sessionDestroy = new SessionController();

    if($sessionDestroy->checkSession('admin'))
    {
        $sessionDestroy->sessionDestroy('admin');
        header('location: '.BASE_URL.'admin/login');
    }
    elseif($sessionDestroy->checkSession('user')){

        $sessionDestroy->sessionDestroy('user');
        header('location: '.BASE_URL.'login');

    }
    $sessionDestroy->setSession('success','Logout successfully.');

}
elseif (isset($_GET['a']) == 'logout')
{

    $sessionDestroy->sessionDestroy('admin');

    header('location: '.BASE_URL.'login');
    $sessionDestroy->setSession('success','Logout successfully.');

}
include_once (__DIR__."/../view/auth/login.php");