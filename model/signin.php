<?php
include_once(__DIR__ . "/include.php");

if (isset($_POST['login_btn'])) {

    $loginControllerObj = new LoginController($_POST);

    $loginControllerObj->checkValidateUser();
}

include_once(__DIR__ . "/../view/auth/login.php");