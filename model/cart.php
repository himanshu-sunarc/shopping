<?php
/**
 * Created by PhpStorm.
 * User: sunarca
 * Date: 26/10/17
 * Time: 12:13 PM
 */

// Empty the cart
include_once(__DIR__."/include.php");

$msg  = '';
if(isset($_SESSION["user"]["user_id"])){
//    var_dump('tyu'); exit;

    $option = [
        'cartMaxItem' => 0,
        // Set maximum quantity allowed per item to 99
        'itemMaxQuantity' => 500,
        // Do not use cookie, cart data will lost when browser is closed
        'useCookie' => false,
    ];
}
else{

$option = [];
}
$cart = new CartController($option);
$sessionObj = new SessionController();
$productListObject  = new ProductController();
$product_array = $productListObject->getProductList();
if (isset($_POST['empty'])) {
    $cart->clear();
}
if(isset($_POST['id'])) {
    $productById = $productListObject->getSingleProductByID($_POST['id']);
}// Add item
if (isset($_POST['add'])) {

    $checkQuantity = $cart->chckQuantityAction($productById[0]["id"],$_POST['qty']);
    if($checkQuantity){
        if(!$cart->add($productById[0]["id"], $_POST['qty'], ['price' => $productById[0]["price"]])) {

            $msg = "Insufficient quantity";
        }
        else{
            $msg = "Product has been added to cart";
        }
    }
    else{
        $a = 'shop';
        $msg = "Insufficient quantity";

    }
}

if (isset($_POST['checkout'])) {

    if ($sessionObj->checkSession('user')) {
//        var_dump($productById); exit;
        $cart->checkoutCart();
//       $allCartItem =  $cart->getCustomerCartItems($_SESSION["user"]["user_id"]);
    }
    else{
        $checkUserObj = new CheckUserController();
        $checkUserObj->checkUserGuestOrRegistered();
    }

}

// Update item
if (isset($_POST['update'])) {
    $checkQuantity = $cart->chckQuantityAction($productById[0]["id"],$_POST['qty']);
    if($checkQuantity) {
        $cart->update($productById[0]["id"], $_POST['qty'], [
            'price' => $productById[0]["price"]
        ]);
        $msg = "Cart has been updated successfully";

    }
    else{
        $msg = "Insufficient quantity";
    }
}
// Remove item
if (isset($_POST['remove'])) {
    $cart->remove($productById[0]["id"], [
        'price' => $productById[0]["price"]
    ]);
    $msg = "Cart Item has been cleared successfully";
}// Remove item

include_once (__DIR__."/../view/cart/cart.php");
