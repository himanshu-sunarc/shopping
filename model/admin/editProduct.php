<?php
include(__DIR__."/../include.php");
$objSessionController=new SessionController();
$isLogin=$objSessionController->checkSession("admin");
if(!$isLogin)
{
    header('Location: '.BASE_URL.'admin/?a=login');
}
$obj=new ProductController();
$product_single_data=$obj->editProduct();
$response=$product_single_data;
if($response=='' || count($response)<=0)
    $response=array();
include (__DIR__."/../../view/admin/editProduct.php");
?>