<?php

include(__DIR__."/../include.php");


if (isset($_GET['a']) == 'logout')
{
    $sessionDestroy = new SessionController();

    if($sessionDestroy->checkSession('admin'))
    {
        $sessionDestroy->sessionDestroy('admin');
        header('location: '.BASE_URL.'admin/login');
    }

    $sessionDestroy->setSession('success','Logout successfully.');
}

include_once (__DIR__."/../admin/login.php");