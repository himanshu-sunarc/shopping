<?php
include(__DIR__."/../include.php");
$objSessionController=new SessionController();
$isLogin=$objSessionController->checkSession("admin");
if(!$isLogin)
{
    header('Location: '.BASE_URL.'admin/?a=login');
}
$obj_helper=new ProductController();
$response=$obj_helper->createProduct();
if($response=='' || count($response)<=0)
    $response=array();
include (__DIR__."/../../view/admin/createProduct.php");
?>