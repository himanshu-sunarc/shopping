<?php
include(__DIR__."/../include.php");
$objSessionController=new SessionController();
$isLogin=$objSessionController->checkSession("admin");
if(!$isLogin)
{
   header('Location: '.BASE_URL.'login');
}
$obj=new ProductController();
$response=$obj->productGrid();
$objProductController=new ProductController();
$product_grid_data=$objProductController->getProductList();
include (__DIR__."/../../view/admin/productGrid.php");
?>