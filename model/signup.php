<?php

include_once (__DIR__."/include.php");

if (isset($_POST['signup_btn']))
{
    $registerControllerObj= new RegistrationController($_POST);

    $validated =  $registerControllerObj->validateUser();

    $isEmailAvailableOrNot = $registerControllerObj->getEmail();
    $isEmailAvailableOrNot = $registerControllerObj->checkErrorNotAvailable();

}

include_once (__DIR__."/../view/auth/registration.php");