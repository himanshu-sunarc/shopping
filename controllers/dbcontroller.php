<?php
class DBController {
	private $host = HOST;
	private $user = USER;
	private $password = PASSWORD;
	private $database = DATABASE;
	private $conn;
	
	function __construct() {
		$this->conn = $this->connectDB();
	}
	
	function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $conn;
	}
	
	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
//		var_dump($result);exit;

		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}
		if(!empty($resultset))
			return $resultset;
	}
	
	function insertRow($query){
		return mysqli_query($this->conn,$query);

	}

	function updateRow($query){
		return mysqli_query($this->conn,$query);

	}
	
	function numRows($query) {
		$result  = mysqli_query($this->conn,$query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
}
?>