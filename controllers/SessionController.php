<?php

class SessionController
{
    public function sessionDestroy($data)
    {
        unset($_SESSION[$data]);
    }
    public function setSession($key, $data)
    {
        $_SESSION[$key] = $data;
    }

    public function getSession($key = null)
    {
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }

        return false;
    }

    public function checkSession($key = null)
    {
        if(isset($_SESSION[$key])){
            return true;
        }

        return false;
    }
}
   
