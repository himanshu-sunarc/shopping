<?php


class LoginController extends AuthController
{

    private $validationHandle;
    private $post;
    public $session;

    public function __construct($post)
    {
        parent::__construct();
        $this->validationHandle = new GUMP();
        $this->session = new SessionController();
        $this->post = $post;
    }

    public function checkValidateUser(){
        $validated = $this->validateUser();

        if (empty($validated->errors())) {

            $userByLogin = $this->validateUserDetails($this->post['email'],$this->post['password'],false);
            
            if ($userByLogin) {

                $session=  [
                    'user_id' => $userByLogin[0]['id'],
                    'user_name' => $userByLogin[0]['user_name'],
                    'name' => $userByLogin[0]['name'],
                    'is_admin' => $userByLogin[0]['role_id']
                ];

                $this->session->setSession('user',$session);
                $this->session->setSession('success',"User login successfully");

                header('location: '.BASE_URL);
                exit();

            } else {
                $this->session->setSession('error',"Please check your email or password");
            }
        } else {
            $this->session->setSession('error_msg', $validated->get_errors_array());
        }
    }

    public function validateUser()
    {
        $data = array(
            'email'	      => $this->post['email'],
            'password' 	  => $this->post['password'],
        );

        $rules = array(
            'email'       => 'required|valid_email',
            'password'    => 'required',
        );

        $this->validationHandle->validate($data, $rules);

        return $this->validationHandle;
    }

        /*public function getUserInfo(){

            $email = $this->post['email'];
            $userByLogin = $this->dbHandle->runQuery("SELECT users.*,roles.id FROM users INNER JOIN roles ON users.role_id = roles.id WHERE users.email='$email' AND users.role_id= 2 ");

            return $userByLogin;
//            return $this->dbHandle;

        }*/


}