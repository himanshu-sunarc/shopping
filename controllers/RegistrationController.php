<?php

class RegistrationController
{
    private $dbHandle;
    private $validationHandle;
    private $post;
    private $session;

    public function __construct($post)
    {
        $this->dbHandle = new DBController();
        $this->validationHandle = new GUMP();
        $this->session = new SessionController();
        $this->post = $post;
    }

    public function checkErrorNotAvailable(){

        $this->validateUser();

        if(empty($this->validationHandle->errors()) )
        {
            $isAlredayAvailable = $this->isAlreadyRegisteredOrNot();

            if ($isAlredayAvailable == '') {
                $this->createUser();
                $this->session->setSession('success', 'You signup successfully');
                header('location: '.BASE_URL.'login');
                exit;
            }
            else{

                $this->session->setSession('error', 'This email is already registered with us.');
            }
        }
        else
        {
            $this->session->setSession('error_msg',$this->validationHandle->get_errors_array());
        }
    }

    public function validateUser()
    {

        $data = array(
                'name' => $this->post['name'],
                'user_name' => $this->post['user_name'],
                'email' => $this->post['email'],
                'password' => $this->post['password'],
                'confirm_password' => $this->post['confirm_password'],

            );

            $rules = array(
                'name' => 'required|max_len,100',
                'user_name' => 'required|alpha_numeric',
                'email' => 'required|valid_email',
                'password' => 'required',
                'confirm_password' => 'equalsfield,password',
            );

             $this->validationHandle->validate($data, $rules);
            return $this->validationHandle;

        }

    public function isAlreadyRegisteredOrNot(){

        $email = $this->post['email'] ;
        $isEmailAvailable = $this->dbHandle->runQuery("SELECT email FROM users WHERE email= '$email'");
        $isEmailAvailable= $isEmailAvailable[0]['email'];

        return $isEmailAvailable;

    }
    public function createUser() {

        $name = $this->post['name'];
        $email = $this->post['email'];
        $userName = $this->post['user_name'];
        $password = md5($this->post['password']);
        $this->dbHandle->insertRow("INSERT INTO users (name, email, user_name, password, role_id) VALUES ('$name','$email','$userName','$password',2)");

        return $this->dbHandle;
    }

        public function getEmail(){

            $email = $this->post['email'] ;
            $isEmailAvailableOrNot = $this->dbHandle->runQuery("SELECT email FROM users WHERE email= '$email'");

            return $isEmailAvailableOrNot[0]['email'];

        }

}