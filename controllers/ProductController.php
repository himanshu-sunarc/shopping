<?php
//session_start();
class ProductController
{
    private $dbHandle;
    public function __construct()
    {
        $this->dbHandle = new DBController();
    }

    public function createProject($folderpath,$prod_name,$prod_code,$price,$qty){

       $query= "insert into products (name,code,image,price,qty) VALUES ('$prod_name','$prod_code','$folderpath','$price','$qty')";
        //mysqli_query($this->dbHandle,$query);
        return $this->dbHandle->insertRow($query);
    }

    public function updateProject($folderpath,$prod_name,$prod_code,$price,$qty,$prod_id){

        $query= "update products set name='$prod_name',code='$prod_code',image='$folderpath',price='$price',qty='$qty' where id=".$prod_id;
        //mysqli_query($this->dbHandle,$query);
        return $this->dbHandle->updateRow($query);
    }
    public function deleteProduct($prod_id)
    {

        return $this->dbHandle->runQuery("Delete from products where id=$prod_id");
    }
    public function getProductList(){
        return $this->dbHandle->runQuery("SELECT * FROM products ORDER BY id ASC");
    }

    public function getSingleProductByID($productID){
        return $this->dbHandle->runQuery("SELECT * FROM products  where id=".$productID);
    }

    public function validateProduct($POST,$update='',$prod_id=''){
        $validator=new GUMP();
        $error=false;
        $_POST['validate'] = array(
            'prod_name' => $_POST['prod_name'],
            'prod_code' => $_POST['prod_code']);

        $rules = array(
            'prod_name' => 'required',
            'prod_code' => 'required');

        $validated = $validator->validate($_POST['validate'], $rules);

// Check if validation was successful
        if($validated === TRUE)
        {
            if(isset($_FILES['company_logo']['tmp_name']) && !empty($_FILES['company_logo']['tmp_name'])){
                $_FILES["company_logo"]["name"] = $_FILES["company_logo"]["name"];
                $image_name = $_FILES["company_logo"]["name"];
                $source_image = $_FILES["company_logo"]["tmp_name"];
//        if( $_FILES["company_logo"]['size']>2048000)
//        {
//            $filename = compress_image($_FILES["company_logo"]["tmp_name"], $url, 2048000);
//        }

                $filename = time() . "_" . $image_name;
                $folderpath = UPLOAD_PATH. $filename;

                $header_path=IMAGE_SHOW . $filename;

                if (move_uploaded_file($_FILES['company_logo']['tmp_name'], $folderpath)) {

                }

            }
            echo "Data added successfully...";
        }else{
            echo "There were errors with the data you provided:\n";
            $error=true;
            return $validator->get_errors_array();
        }
        $header_path = '';



        $prod_name=$POST['prod_name']?$POST['prod_name']:'';
        $prod_code=$POST['prod_code']?$POST['prod_code']:'';
        $price=$POST['price']?$POST['price']:'';
        $qty=$POST['qty']?$POST['qty']:'';
        if(!$error)
        {
            if($update)
            {
                $login_return_data=$this->updateProject($header_path,$prod_name,$prod_code,$price,$qty,$prod_id);
                return array();
            }
            else
            {
                $login_return_data=$this->createProject($header_path,$prod_name,$prod_code,$price,$qty);
                return array();
            }

        }

    }
    
    public function productGrid()
    {
        $sessionObj=new SessionController();
        if(isset($_POST['createProduct']))
        {
            header('Location: '.BASE_URL.'admin/?a=createProduct');
        }
//        if(isset($_POST['logout']))
//        {
//            $sessionData=$sessionObj->sessionDestroy('admin');
//            header('Location: '.BASE_URL.'admin/?a=login');
//        }
        return true;
    }



    /**
     *
     */
    public function createProduct()
    {
        $sessionObj=new SessionController();
        if(isset($_POST['logout']))
        {
            $sessionObj->sessionDestroy('admin');
            header('Location: '.BASE_URL.'admin/?a=login');
        }

        if(isset($_POST['product']))
        {
            header('Location: '.BASE_URL.'admin/?a=productGrid');
        }
        if(isset($_POST['submit']))
        {
            return $this->validateProduct($_POST);

        }

    }

    public function editProduct()
    {
        $sessionObj=new SessionController();
        if(isset($_POST['logout']))
        {   $sessionObj->sessionDestroy('admin');
            header('Location: '.BASE_URL.'admin/?a=login');
        }
        if(isset($_POST['product']))
        {
            header('Location: '.BASE_URL.'admin/?a=productGrid');
        }
        if(isset($_GET['id'])) {
            $obj = new ProductController();
            $product_single_data = $obj->getSingleProductByID($_GET['id']);
            if (isset($_POST['submit'])) {
                $response = $obj->validateProduct($_POST,1,$_GET['id']);
                $product_single_data = $obj->getSingleProductByID($_GET['id']);
            }
            return $product_single_data;
        }
    }

}