<?php
class CheckUserController
{
    private $dbHandle;
    public function __construct()
    {
        $this->dbHandle = new DBController();
    }

    public function checkUserGuestOrRegistered(){

        header('Location: '.BASE_URL.'checkuser');
    }
    public function checkIfUserAvailable(){

        header('Location: '.BASE_URL.'login');
    }


}