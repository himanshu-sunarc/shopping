<?php
include(__DIR__ . "/../model/include.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class CartController extends Cart
{
    public $mailer;
    private $db_handler;
    public $product_contoller;

    public function __construct($options = [])
    {
        if (empty($options)) {
            $options = [
                'cartMaxItem' => 0,
                // Set maximum quantity allowed per item to 99
                'itemMaxQuantity' => 500,
                // Do not use cookie, cart data will lost when browser is closed
                'useCookie' => true,
            ];
        }

        parent::__construct($options);
        $this->db_handler = new DBController();
        $this->mailer = new PHPMailer(true);
        $this->product_contoller = new ProductController();
    }

    /*Check quantity is available or not*/

    public function add($id, $quantity = 1, $attributes = [])
    {
        $quantity = (preg_match('/^\d+$/', $quantity)) ? $quantity : 1;
        $attributes = (is_array($attributes)) ? array_filter($attributes) : [$attributes];
        $hash = md5(json_encode($attributes));
        if (isset($this->items[$id])) {
            $index = 0;
            foreach ($this->items[$id] as $item) {

                if ($item['hash'] == $hash) {
                    $checkQuantity = $this->chckQuantityAction($id, ($this->items[$id][$index]['quantity'] + $quantity));
                    if ($checkQuantity) {
                        $this->items[$id][$index]['quantity'] += $quantity;
                        $this->items[$id][$index]['quantity'] = ($this->items[$id][$index]['quantity'] > $this->itemMaxQuantity) ? $this->itemMaxQuantity : $this->items[$id][$index]['quantity'];
                        $this->write();
                        return true;
                    } else {
                        return false;
                    }
                }
                ++$index;
            }
        }
        $this->items[$id][] = [
            'quantity' => ($quantity > $this->itemMaxQuantity) ? $this->itemMaxQuantity : $quantity,
            'hash' => $hash,
            'attributes' => $attributes,
        ];
        if(isset($_SESSION['user']['user_id'])){
            $userId = $_SESSION['user']['user_id'];
            $cartItems = $this->db_handler->runQuery("Select product_id,qty from cart_item where is_ordered = 'N' and user_id = ".$_SESSION['user']['user_id']);
            $productIds = '';
            if($cartItems) {
                foreach ($cartItems as $item => $value) {
                    if ($value["product_id"]) {
                        $productIds[] = $value["product_id"];
                    }
                }
            }
            foreach ($this->items as $key => $value){
                if($productIds == '' || !in_array($key,$productIds)) {
                   $this->db_handler->runQuery("Insert into cart_item (product_id,cart_key, user_id, qty, price) VALUES (" . $key . ",'" .$value[0]["hash"] ."'," . $_SESSION['user']['user_id'] . "," . $value[0]["quantity"] . "," . $value[0]["attributes"]["price"] . ")");
                }
                else{
                    $this->db_handler->updateRow("Update cart_item set qty = ".$value[0]["quantity"]);

                }
            }
        }

        $this->write();

        return true;
    }

    /*This function is used for add product in cart*/

    public function chckQuantityAction($productId, $productQty)
    {
        $productById = $this->db_handler->runQuery("SELECT * FROM products WHERE id='" . $productId . "'");
        if ($productQty > $productById[0]["qty"]) {
            return false;
        } else {
            return true;
        }
    }

    public function write()
    {
        //if ($this->useCookie)
        if ($this->useCookie) {
            setcookie($this->cartId, json_encode(array_filter($this->items)), time() + 604800);
        } else {
            $_SESSION[$this->cartId] = json_encode(array_filter($this->items));
        }
    }

    public function getCustomerCartItems($userId = ''){
        if($userId != '') {
            $cartItems = $this->db_handler->runQuery("Select * from cart_item where is_ordered = 'N' and user_id = ".$userId);
//            var_dump($cartItems);exit;
            $index = 0;
            $this->items = '';
            foreach ($cartItems as $key => $value){
               $this->items[$value["product_id"]][] = ["quantity" => $value["qty"], "attributes" => ["price" => $value["price"]], "hash"=>$value["cart_key"]];
                ++$index;
            }
            return $this->items;
        }
        else{
            return $this->items;
        }
    }

    /**
     * Remove item from cart.
     *
     * @param string $id
     * @param array  $attributes
     *
     * @return bool
     */
    public function remove($id, $attributes = [])
    {
        if (!isset($this->items[$id])) {
            return false;
        }
        if (empty($attributes)) {
            $this->db_handler->runQuery("Delete from cart_item where product_id = ".$id);
            unset($this->items[$id]);
            $this->write();

            return true;
        } else {
            $hash = md5(json_encode(array_filter($attributes)));
            $index = 0;
            foreach ($this->items[$id] as $item) {
                if ($item['hash'] == $hash) {
                    $this->db_handler->runQuery("Delete from cart_item where product_id = ".$id);
                    unset($this->items[$id][$index]);
                    $this->write();

                    return true;
                }
                ++$index;
            }
        }
        return false;
    }

    public function update($id, $quantity = 1, $attributes = [])
    {
        $quantity = (preg_match('/^\d+$/', $quantity)) ? $quantity : 1;

        if (0 == $quantity) {
            $this->remove($id, $attributes);

            return true;
        }

        if (isset($this->items[$id])) {
            $hash = md5(json_encode(array_filter($attributes)));

            $index = 0;
            foreach ($this->items[$id] as $item) {
                if ($item['hash'] == $hash) {
                    $this->items[$id][$index]['quantity'] = $quantity;
                    $this->items[$id][$index]['quantity'] = ($this->items[$id][$index]['quantity'] > $this->itemMaxQuantity) ? $this->itemMaxQuantity : $this->items[$id][$index]['quantity'];

                    $this->write();

                    return true;
                }
                ++$index;
            }
        }

        return false;
    }


    public function checkoutCart()
    {
//        $allItems = $this->getItems();
        if(isset($_SESSION["user"]["user_id"]) && empty($_COOKIE)){
//            var_dump($_COOKIE);exit;

            $allItems = $this->getCustomerCartItems($_SESSION["user"]["user_id"]);

//            return $allItems;

        }
        elseif(isset($_SESSION["user"]["user_id"]) && !empty($_COOKIE)){

//            $allItems = $this->getItems();
            $allItems = $_SESSION['guest_cart'];
//            return $allItems;
            var_dump($allItems);exit;

        }
//        echo '<pre>';print_r($allItems);exit;
        $personDetail = $this->db_handler->runQuery("Select email from users where role_id = 1");
        $superAdminEmail = $personDetail[0]["email"];

        try {

            $this->mailer->isSMTP();                                      // Set mailer to use SMTP
            $this->mailer->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mailer->Username = 'johnarc5@gmail.com';                 // SMTP username
            $this->mailer->Password = 'sunarc123';                           // SMTP password
            $subject = "Shopping details";
            $name = 'Vardhan';
            $to = 'riyaz.ahamad@sunarctechnologies.com';
            $this->mailer->addAddress($to);     // Add a recipient
            $this->mailer->addCC($superAdminEmail);
            $this->mailer->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $this->mailer->Port = 587;                                    // TCP port to connect to
            $this->mailer->isHTML(true);
            //Recipients
            $this->mailer->setFrom('johnarc5@gmail.com', 'Shopping Team');

            $body = "	
	Dear User,
	<p>Thanks for shopping with us.</p>
			 <table width='100%' align='center' >
	        <tr> 
	            <th align='left'>#Item Name</th> 
	            <th align='left'>Quantity</th>
	            <th align='left'>Price</th>
	        </tr>
            <tr>";


            foreach ($allItems as $id => $items) {

                foreach ($items as $item) {
                    $hash = $item['hash'];
                    $productDetail = $this->product_contoller->getSingleProductByID($id);
                    $body.= "<tr>";
                    foreach ($productDetail as $key => $value) {
                        $body .=  "<td>".$productDetail[$key]["name"]."</td>";
                    }
                    $body .= "<td class='text-center'>". $item['quantity']."</td>
                     <td class='text-right'>$". $item['attributes']['price']."</td>";
                }
            }

            $body .= "</tr> <tr style='border: 1px solid black; background-color: bisque'>
                <th colspan='2' style='text-align: center'>Total price</th>
             <td >$".$this->getAttributeTotal()."</td>";


            $body.=  "</tr> 
            <tr></tr> <tr></tr>
            <tr></tr> <tr></tr>
             <tr > <td> Regards, </td> </tr>
			<tr></tr> <tr></tr> <tr></tr>
			<tr > <td > Shopping Team. </td> </tr>
			</table> ";

            $this->mailer->Body = $body;

//            $aa = $this->db_handler->runQuery("DELETE FROM cart_item WHERE cart_key = '$hash' and user_id=".$_SESSION['user']['user_id']);
            $sessionObj = new SessionController();
            if ($sessionObj->checkSession('user')){
                $aa = $this->db_handler->runQuery("DELETE FROM cart_item WHERE cart_key = '$hash'");
            }
//            var_dump($aa); exit();
                // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: <johnarc5@gmail.com>' . "\r\n";

            $this->mailer->send();
            $this->destroy();
            unset($_COOKIE);
            unset($_SESSION['guest_cart']);

            return true;
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $this->mailer->ErrorInfo;
        }
    }

}