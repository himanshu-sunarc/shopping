<?php include(__DIR__ . "/../inc/header.php");
if ($msg != '' && !$cart->isEmpty()) {
    ?>
    <div class="alert alert-warning">
        <?php echo $msg; ?>
    </div>
<?php }
if (!$cart->isEmpty()) {
  //  $allItems = $cart->getItems();
    $userId = '';
    if(isset($_SESSION["user"]["user_id"])){
      $userId = $_SESSION["user"]["user_id"];
    }

//    $allItems = $cart->getCustomerCartItems($userId) ? $cart->getCustomerCartItems($userId) : $cart->getItems();
    $allItems = $cart->getItems() ? $cart->getItems():$cart->getCustomerCartItems($userId);
//            var_dump($cart->getCustomerCartItems($userId));exit;
    $_SESSION['guest_cart']= $cart->getItems();
    ?>
    <form action="" method="post">

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-md-4">Product</th>
            <th class="col-md-3 text-center">Quantity</th>
            <th class="col-md-2 text-right">Price</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($allItems as $id => $items) {
            foreach ($items as $item) {
                foreach ($product_array as $key => $value) {
                    if ($id == $product_array[$key]["id"]) {
                        break;
                    }
                } ?>
                <tr>
                    <td><?php echo $product_array[$key]["name"]; ?></td>
                    <td class="text-center">
                        <div class="form-group">
                            <input type="number" value="<?php echo $item['quantity'] ?>"
                                   class="form-control input-sm quantity pull-left" style="width:100px">
                            <div class="pull-righ">
                                <button class="btn btn-default btn-sm btn-update" data-id="<?php echo $id ?>"><i
                                            class="fa fa-refresh"></i> Update
                                </button>
                                &nbsp;&nbsp;
                                <button class="btn btn-sm btn-danger btn-remove" data-id="<?php echo $id; ?>"><i
                                            class="fa fa-trash"></i>&nbsp;Remove
                                </button>
                            </div>
                        </div>
                    </td>
                    <td class="text-right">$<?php echo $item['attributes']['price'] ?></td>

<!--                    <input type="hidden" value="--><?php //echo $product_array[$key]['name'] ; ?><!--" name="product_name_td[]">-->
<!--                    <input type="hidden" value="--><?php //echo $item['attributes']['price'] ; ?><!--" name="product_price_td[]">-->
<!--                    <input type="hidden" value="--><?php //echo $item['quantity'] ; ?><!--" name="product_qty_td[]">-->
                </tr>
            <?php }
        }
        ?>

        </tbody>
    </table>

        <?php
//        if ($cart->getAttributeTotal('price') > 0)
//        {
            ?>
            <div class="text-right">
                <h3>Total:<br/>$<?php echo number_format($cart->getAttributeTotal('price'), 2, '.', ',') ?></h3>
            </div>

            <div class="pull-left">
                <button class="btn btn-danger btn-empty-cart btn-sm">Empty Cart</button>
            </div>
            <div class="pull-right text-right">
                <a href="<?php echo BASE_URL ?>" class="btn btn-default btn-sm">Continue Shopping</a>
                <button type="submit" name="checkout" class="btn btn-primary btn-sm">Checkout</button>
            </div>
        <?php
        /*} else {*/ ?>
<!--            <h3 class="text-center">No data available.</h3>-->
        <?php /*}*/
        ?>

    </form>
<?php } else {
    ?>
    <div class="alert alert-warning">
        <i class="fa fa-info-circle"></i> There are no items in the cart.
    </div>
<?php }
include(__DIR__ . "/../inc/footer.php"); ?>

