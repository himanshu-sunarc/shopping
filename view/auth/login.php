<?php

include(__DIR__ . "/../inc/header.php");
$session = new SessionController();

if (!$session->checkSession('user'))
{
?>
<style>
    .error{
        color: red;
    }
</style>

    <section id="login" style="margin-top: 8%;">
        <div class="">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3" style="background-color: #cccccc">
                    <div class="form-wrap">
                        <h3 style="margin-left: 27%;margin-bottom: 25px;">Log in with your email account</h3>
                        <?php
                        if ($session->checkSession('error')) {
                            echo '<table name="error" cellspacing="0" cellpadding="0" border="0" align="center" width="75%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $session->getSession('error');

                            echo '</div></td></tr></tbody></table><br>';
                            $session->sessionDestroy('error');

                        }
                        if ($session->checkSession('success')) {
                            echo '<table name="success" cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				        <div class="alert alert-success alert-dismissable">
				        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                            echo $session->getSession('success');
                            echo '</div></td></tr></tbody></table><br>';
                            $session->sessionDestroy('success');
                        }

                        if ($session->checkSession('error_msg')) {
                            echo '<table cellspacing="0" style="margin-left: 90px;" cellpadding="0" border="0" align="center" width="80%" ><tbody><tr><td colspan="6"  align="center">
				            <div class="alert alert-danger alert-dismissable">
				            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                            foreach ($session->getSession('error_msg') as $error) {
                                echo $error . '<br>';
                            }

                            echo '</div></td></tr></tbody></table><br>';
                            $session->sessionDestroy('error_msg');
                        }
                        ?>
                        <form style="width: 70%;margin-left: 18%;" role="form" action="" method="post" id="loginForm" autocomplete="on">
                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>"
                                       name="email" id="email" class="form-control" placeholder="somebody@example.com">
                            </div>
                            <div class="form-group">
                                <label for="key" class="sr-only">Password</label>
                                <input type="password" name="password" id="key" class="form-control"
                                       placeholder="Password">
                            </div>

                            <input type="submit" id="btn-login" name="login_btn" class="btn btn-custom btn-primary btn-sm btn-block"
                                   value="Log in">
                        </form>
                        <!--                    <a href="javascript:;" class="forget" data-toggle="modal" data-target=".forget-modal">Forgot your password?</a>-->
                        <a style="margin-left: 18%;" href="index.php?m=signup" class="registration">Create an account</a>
<!--                        <a style="margin-left: 34%;" href="index.php?m=forgot_password" class="registration">Forgot password?</a>-->
                        <hr>
                    </div>
                </div> <!-- /.col-xs-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
<?php

unset($_POST);

?>
    <script>
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#loginForm").validate({
            rules: {

                email: {
                    required: true,
                    email: true
                },
                password: "required"
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    </script>

<?php

include(__DIR__ . "/../inc/footer.php");
}
else{

    header('Location:'.BASE_URL.'');
}
?>