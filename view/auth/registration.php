<?php

include (__DIR__."/../inc/header.php");

if (!$session->checkSession('user')) {
    ?>
    <style>

        *[role="form"] {
            max-width: 610px;
            padding: 15px;
            margin: 0 auto;
            background-color: #fff;
            border-radius: 0.3em;
        }

        *[role="form"] h2 {
            margin-left: 5em;
            margin-bottom: 1em;
        }

        .error {
            color: red;
        }

    </style>

    <div class="col-xs-6 col-xs-offset-3" style="margin-top: 4%;">
        <!--    <form class="form-horizontal" method="post" action="model/authAction.php" role="form" style="background-color: #cccccc">-->
        <form class="form-horizontal" action="" id="signupForm" method="post" role="form"
              style="background-color: #cccccc">

            <h2 class="">Registration Form</h2>
            <?php
            $session = new SessionController();

            if ($session->checkSession('error')) {
                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                echo $session->getSession('error');
                echo '</div></td></tr></tbody></table><br>';
                $session->sessionDestroy('error');
            }
            if ($session->checkSession('success')) {
                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                echo $session->getSession('success');
                echo '</div></td></tr></tbody></table><br>';
                $session->sessionDestroy('success');
            }

            if ($session->checkSession('error_msg')) {
                echo '<table cellspacing="0" style="margin-left: 90px;" cellpadding="0" border="0" align="center" width="80%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                foreach ($session->getSession('error_msg') as $error) {
                    echo $error . '<br>';
                }

                echo '</div></td></tr></tbody></table><br>';
                $session->sessionDestroy('error_msg');
            }
            ?>
            <br>
            <div class="form-group">
                <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                <div class="col-sm-8">
                    <input type="text" value="<?php if (isset($_POST['name'])) echo $_POST['name']; ?>" name="name"
                           id="name" placeholder="Full Name" class="form-control" autofocus>
                    <!--                <span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>-->
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>" id="email"
                           placeholder="Email" name="email" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">User Name</label>
                <div class="col-sm-8">
                    <input type="text" value="<?php if (isset($_POST['user_name'])) echo $_POST['user_name']; ?>"
                           name="user_name" id="user_name" placeholder="User name" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-8">
                    <input type="password" name="password" id="password" placeholder="Password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-8">
                    <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm password"
                           class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <button type="submit" name="signup_btn" class="btn btn-primary btn-sm btn-block">Register</button>
                </div>
            </div>
        </form> <!-- /form -->
    </div> <!-- ./container -->

    <script>
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#signupForm").validate({
            rules: {
                name: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true
                },
                user_name: {
                    required: true,
                },
                password: "required",
                confirm_password: {
                    equalTo: "#password"
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    </script>

    <?php

    include(__DIR__ . "/../inc/footer.php");

}
else{
    header('Location:'.BASE_URL.'');
}
?>