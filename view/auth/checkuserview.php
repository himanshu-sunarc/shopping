<?php

include (__DIR__."/../inc/header.php");

//if (!$session->checkSession('user')) {
    ?>

    <div class="col-xs-6 col-xs-offset-3" style="margin-top: 4%;">
        <!--    <form class="form-horizontal" method="post" action="model/authAction.php" role="form" style="background-color: #cccccc">-->
        <form class="form-horizontal" action="" id="signupForm" method="post" role="form"
              style="background-color: #cccccc">

            <h2 class="text-center">Provide information to us.</h2>
            <?php
            $session = new SessionController();

            if ($session->checkSession('error')) {
                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                echo $session->getSession('error');
                echo '</div></td></tr></tbody></table><br>';
                $session->sessionDestroy('error');
            }
            if ($session->checkSession('success')) {
                echo '<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                echo $session->getSession('success');
                echo '</div></td></tr></tbody></table><br>';
                $session->sessionDestroy('success');
            }

            if ($session->checkSession('error_msg')) {
                echo '<table cellspacing="0" style="margin-left: 90px;" cellpadding="0" border="0" align="center" width="80%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

                foreach ($session->getSession('error_msg') as $error) {
                    echo $error . '<br>';
                }

                echo '</div></td></tr></tbody></table><br>';
                $session->sessionDestroy('error_msg');
            }
            ?>
            <br>
            <div class="form-group">
                <label class="control-label col-sm-3">Select Option <span class="text-danger">*</span></label>
                <div class="col-md-8 col-sm-9">
                    <label>
                        <input name="user_already_available" type="radio" value="Male" checked>
                        I am already registered user. </label>
                      
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">Select Option <span class="text-danger">*</span></label>
                <div class="col-md-8 col-sm-9">
                    <label>
                        <input name="user_not_available" type="radio" value="Female" >
                        I want to signup
                    </label>
                      
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-3">
                    <button type="submit" name="user_availibility_before_checkout_btn" class="btn btn-primary btn-sm btn-block">Register</button>
                </div>
            </div>
        </form> <!-- /form -->
    </div> <!-- ./container -->

    <script>
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#signupForm").validate({
            rules: {
                name: {
                    required: true,
                    lettersonly: true
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    </script>

    <?php

    include(__DIR__ . "/../inc/footer.php");

//}
//else{
//    header('Location:'.BASE_URL.'');
//}
?>