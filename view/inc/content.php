<?php include(__DIR__ . "/../inc/header.php"); ?>
<h1>Products</h1>
<div class="row">
    <?php
    foreach ($product_array as $key => $value) {
        ?>
        <div class="col-md-6" style="border-right: 1px solid darkgray;">
            <h3><?php echo $product_array[$key]["name"]; ?></h3>
            <div style="margin-left: 20px;">
                <div class="pull-left">
                    <img src="<?php if ($product_array[$key]["image"]) {
                        echo $product_array[$key]["image"];
                    } else {
                        echo BASE_URL . "public/images/product_image.png";
                    } ?>" border="0" title="<?php echo $product_array[$key]["name"] ?>"
                         style="width: 100px; height: 75px;"/>
                </div>
                <div class="pull-right">
                    <h4><?php echo $product_array[$key]["price"] ?></h4>
                    <form>
                        <input type="hidden" value="<?php echo $product_array[$key]["id"] ?>" class="product-id"/>
                        <?php if ($product_array[$key]["qty"] > 0) { ?>
                        <div class="form-group">
                            <label>Quantity:</label>
                            <input type="number" value="1" class="form-control input-sm quantity"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger btn-sm add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart </button>

                            <?php }
                            else {
                                ?>
                                <span> Out of Stock</span>
                            <?php } ?>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php }

    ?>
</div>
<?php include(__DIR__ . "/../inc/footer.php"); ?>
