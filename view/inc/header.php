<html lang="en">
<head>
    <title>Online Shopping</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cosmo/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script src="<?php echo BASE_URL?>/public/js/home.js"></script>
<!--    <script src="--><?php //echo BASE_URL?><!--/public/js/checkout.js"></script>-->
    <style>
        body{margin-top:50px;margin-bottom:200px}
    </style>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="<?php echo BASE_URL; ?>" class="navbar-brand">Online Shopping</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav pull-right">
                <li><a href="cart" id="li-cart"><i class="fa fa-shopping-cart"></i> Cart (<?php
                        if($model == 'login' || $model == 'signup')
                            $cart = '';
                            $cart = new CartController();
                        echo $cart->getTotalItem(); ?>)</a></li>

            <?php
            $session = new SessionController();
            if ($session->checkSession('user') || $session->checkSession('admin')) { ?>
                <li><a style="color: white;font-weight: bold;" id="btnLogin" href="index.php?m=logout" class="text-bold">Logout</a></li>
                <?php
            }
            else { ?>
                <li><a style="color: white;font-weight: bold;" id="btnLogin" href="login">Login</a></li>
                <li><a style="color: white;font-weight: bold;" id="btnLogin" href="signup">Signup</a></li>

            <?php } ?>
            </ul>
        </div>


    </div>

</div>