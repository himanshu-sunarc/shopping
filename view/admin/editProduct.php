<?php
/**
 * Created by PhpStorm.
 * User: sunarc123
 * Date: 23/10/17
 * Time: 12:30 PM
 */
?>
<!DOCTYPE html>
<html>
<body>
<div class="input_buttons">
</div>
<div class="container">
    <form method="post" action="" enctype="multipart/form-data" id="edit_form">
        <input type="submit" id="logout" name="logout" value="Logout"/>
        <input type="submit" id="product" name="product" value="View products"/>
        <table class="table table-bordered table-responsive">

            <tr>
                <td><label class="control-label">Product name :</label></td>
                <td><input placeholder="Product naae is required" autofocus="autofocus" required="required" class="form-control" type="text" value="<?php echo $product_single_data['0']['name'] ?>" name="prod_name" id="prod_name" placeholder="" /></td>
            </tr>
            <tr>
                <td><label class="control-label">Product code :</label></td>
                <td><input placeholder="Product code is required" required="required" class="form-control" value="<?php echo $product_single_data['0']['code'] ?>" type="text" name="prod_code" id="prod_code" placeholder=""/></td>
            </tr>
            <tr id="errors">
                <td></td> <td><?php if(array_key_exists("Prod Code",$response)){echo $response['Prod Code'];} ?></td>
            </tr>
            <tr id="errors">
                <td></td> <td><?php if(array_key_exists("Prod Code",$response)){echo $response['Prod Code'];} ?></td>
            </tr>
            <tr>
                <td><label class="control-label">Price :</label></td>
                <td><input class="form-control" value="<?php echo $product_single_data['0']['price'] ?>" type="number" name="price" id="price" placeholder=""   /></td>
            </tr>
            <tr>
                <td><label class="control-label">Qty :</label></td>
                <td><input class="form-control" type="number" value="<?php echo $product_single_data['0']['qty'] ?>" name="qty" id="qty" placeholder=""   /></td>
            </tr>
            <tr>
                <input type="file" name="company_logo"/><?php if(!empty($product_single_data['0']['image'] )): ?> <img id="prod_img"src="<?php echo  $product_single_data['0']['image'] ?>">  <?php endif; ?>
            </tr>
            <tr>
                <td><input style="cursor: pointer" class="form-control" type="submit" name="submit" id="submit" value="Submit"/></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

<script>
    var Jdata=jQuery.noConflict();
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
    Jdata( "#edit_form" ).validate({
        rules: {
            prod_name: {
                required: true,
                lettersonly: true
            },
            prod_code: {
                required: true,
                email: true
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
</script>
