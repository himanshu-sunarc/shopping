<?php
$i=0;
//include (__DIR__."/../inc/header.php");
?>
<form method="post" action="" enctype="multipart/form-data">
<!--    <input type="submit" id="logout" name="logout" value="Logout"/>-->
    <a class="btn btn-info btn-sm" href="?a=logout" id="logout" style="margin-right:15px;border-radius: 5px;" >Logout</a>
    <input class="btn btn-info btn-sm" type="submit" id="createProduct" style="margin-right:15px;border-radius: 5px;" name="createProduct" value="Add new product"/>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">entity id</th>
            <th scope="col">Product name</th>
            <th scope="col">product code</th>
            <th scope="col">image</th>
            <th scope="col">qty</th>
            <th scope="col">price</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(!empty($product_grid_data)){
            foreach($product_grid_data as $value) {
                ?>
                <tr>
                    <td><?php echo  ++$i; ?> </td>
                    <td><?php echo  $value['id'] ?> </td>
                    <td><?php echo  $value['name'] ?> </td>
                    <td><?php echo  $value['code'] ?> </td>
                    <td><?php if(!empty($value['image'] )): ?><img id="prod_img"src="<?php echo  $value['image'] ?>"> <?php endif; ?> </td>
                    <td><?php echo  $value['qty'] ?> </td>
                    <td><?php echo  $value['price'] ?> </td>
                    <td name=<?php echo  $value['id']?>><a href=<?php echo BASE_URL.'admin/?a=editProduct&id='.$value['id']; ?>>Edit </a></td>
                    <td name=<?php echo  $value['id'] ?>><a href=<?php echo BASE_URL.'admin/?a=deleteProduct&id='.$value['id']; ?>>Delete </a></td>

                </tr>

                <?php
            }
        }
        ?>
        </tbody>
    </table>
</form>

<?php

//include(__DIR__ . "/../inc/footer.php");

?>