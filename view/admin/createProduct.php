<?php
/**
 * Created by PhpStorm.
 * User: sunarc123
 * Date: 23/10/17
 * Time: 12:30 PM
 */
?>
<!DOCTYPE html>
<html>
<body>
<div class="container">
    <form method="post" action="" enctype="multipart/form-data">
        <input type="submit" id="logout" name="logout" value="Logout"/>
        <input type="submit" id="product" name="product" value="View products"/>
        <table class="table table-bordered table-responsive">

            <tr>
                <td><label class="control-label">Product name :</label></td>
                <td><input placeholder="Product name is required" class="form-control" type="text" autofocus="autofocus" name="prod_name" id="prod_name" placeholder="" /></td>
            </tr>
            <tr id="errors">
                <td></td> <td><?php if(array_key_exists("Prod Name",$response)){echo $response['Prod Name'];} ?></td>
            </tr>

            <tr>
                <td><label class="control-label">Product code :</label></td>
                <td><input placeholder="Product code is required" class="form-control" type="text" name="prod_code" id="prod_code" placeholder=""/></td>
            </tr>
            <tr id="errors">
                <td></td> <td><?php if(array_key_exists("Prod Code",$response)){echo $response['Prod Code'];} ?></td>
            </tr>
            <tr>
                <td><label class="control-label">Price :</label></td>
                <td><input class="form-control" type="number" name="price" id="price" placeholder=""   /></td>
            </tr>
            <tr>
                <td><label class="control-label">Qty :</label></td>
                <td><input class="form-control" type="number" name="qty" id="qty" placeholder=""   /></td>
            </tr>
            <tr>
               <td> <input type="file" name="company_logo"/></td><td>Please upload file with 2Mb size</td>

            </tr>
            <tr>
                <td><input style="cursor: pointer" class="form-control" type="submit" name="submit" id="submit" value="Submit"/></td>
            </tr>
        </table>

    </form>
</div>
</body>
</html>
