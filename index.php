<?php
$model = '';
if(isset($_GET['m'])){
     $model = $_GET['m'];
}
else{
	$model = 'home';
}
switch ($model)
{
    case "login":
        require_once (__DIR__."/model/signin.php");
        break;

    case "signup":
        require_once (__DIR__."/model/signup.php");
        break;

    case "logout":
        require_once (__DIR__."/model/logout.php");
        break;
	case "home":
		require_once (__DIR__."/model/home.php");
		break;
    case "cart":
        require_once (__DIR__."/model/cart.php");
        break;
    case "checkuser":
    require_once (__DIR__."/model/checkuser.php");
        break;
    case "checkout":
        require_once (__DIR__."/model/checkout.php");
     break;
    default:
        require_once (__DIR__."/model/home.php");
		break;
}

?>
